import React from 'react';
import { Link } from "react-router-dom";
import { ButtonContainer } from "./Button";
import './Navbar.css';
import logo from "../logo.png";
const nav = props => (
  <header className="nav">
    <nav className="navigation">
      <div className="logo">        
        <a href="/">
          <img src={logo} alt="store"/>
        </a>
      </div>
      <div className="nav_logo"><a href="/">AquaBASE Diving Equipment LTD </a></div>>
      <div className="nav_items2">
          <Link to="/cart" className="my_cart">
             <span>
              cart
             </span>
          </Link>
      </div>
      <div className="nav_items1">
        < button class="dropbtn">Dropdown</button>
        <div class="dropdown-content">
          <a href="#">Link 1</a>
          <a href="#">Link 2</a>
          <a href="#">Link 3</a>
        </div>
      </div>
    </nav>
  </header>
); 

export default nav;